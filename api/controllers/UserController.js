const UserService = require('../services/UserService');
const bcrypt = require('bcrypt');

module.exports = class UserController {
    static async login(req, res) {
        // return res.json({kk: 'wdw'})
        try {
            const allUsers = await UserService.getAllUsers();
            if (allUsers.length > 0) {
                return res.status(200).json({title: ".."});
            } else {
                return res.status(200).json({title: "..."});
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getAllUsers(req, res) {
        try {
            const users = await UserService.getAllUsers();
            if (users) {
                return res.status(200).json(users);
            }
            return [];
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async addUser(req, res) {

        const newUser = req.body;
        try {
            newUser.password = await bcrypt.hash(newUser.password, 10)
            const createdUser = await UserService.addUser(newUser);
            return res.json(createdUser)
        } catch (error) {
            console.log(error)
        }
    }

    static async getCaretakers(req, res) {
        try {
            const caretakers = await UserService.getAllCaretakers();
            if (caretakers) {
                return res.status(200).json(caretakers);
            }
            return [];
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getMyPharmacy(req, res) {
        try {
            const userPharmacyId = req.user.pharmacyId;
            const pharmacy = await UserService.getMyPharmacy(userPharmacyId);
            return res.status(200).json(pharmacy);
        } catch (error) {
            return res.status(400).json(error);
        }
    }
}

