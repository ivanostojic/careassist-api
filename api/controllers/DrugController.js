const DrugService = require('../services/DrugService');
const {Op} = require('sequelize')

const removeEmpty = (obj) => {
    let newObj = {};
    Object.keys(obj).forEach((prop) => {
        if (obj[prop] !== '' && obj[prop].length) {
            newObj[prop] = obj[prop];
        }
    });
    return newObj;
};

// const filterData = (data, query) => {
//     return data.filter(item => {
//         for (let key in query) {
//             let b = query[key]
//             let c = item[key].map(it => it.id)
//             console.log(c);
//             if (item[key] === undefined || !query[key].includes(item[key].id)) {
//                 return false;
//             }
//         }
//         return true;
//     });
// };

module.exports = class DrugController {

    static async searchDrugs(req, res) {
        try {
            let filters = removeEmpty(req.body);
            let whereClause = {};
            let whereClauseTags = {};
            let whereClauseShapes = {};
            let whereClauseDrugClasses = {};

            console.log(filters);
            if (filters.genericName) {
                whereClause.genericName = {[Op.iLike]: '%' + filters.genericName + '%'}
            }
            if (filters.brandName) {
                whereClause.brandName = {[Op.iLike]: '%' + filters.brandName + '%'}
            }
            if (filters.year) {
                whereClause.year = new Date(filters.year).getFullYear()
            }
            if (filters.Tags) {
                whereClauseTags.id = {[Op.in]: filters.Tags}
            } else {
                whereClauseTags = null;
            }
            if (filters.Shapes) {
                whereClauseShapes.id = {[Op.in]: filters.Shapes}
            } else {
                whereClauseShapes = null;
            }
            if (filters.DrugClasses) {
                whereClauseDrugClasses.id = {[Op.in]: filters.DrugClasses}
            } else {
                whereClauseDrugClasses = null;
            }
            let drugs = await DrugService.getFilteredDrugs(whereClause, whereClauseTags, whereClauseShapes, whereClauseDrugClasses);
            // if (filters.selectedTags) {
            //     whereClause.tags = {$like: '%' + filters.brandName + '%'}
            // }
            // if (filters.brandName) {
            //     whereClause.brandName = {$like: '%' + filters.brandName + '%'}
            // }
            // console.log(drugs);
            console.log(drugs);
            // console.log(result);
            if (drugs.length > 0) {
                return res.status(200).json(drugs);
            } else {
                return res.status(200).json({title: "There are no drug classes in database"});
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getDrug(req, res) {
        try {
            const drugId = req.params.id
            const userPharmacyId = req.user.pharmacyId;
            const drug = await DrugService.getADrug(drugId, userPharmacyId);
            return res.status(200).json(drug);
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async modifyStock(req, res) {
        try {
            const addStock = req.body.addStock;
            const stockValue = req.body.stockValue;
            const drugId = req.params.id;
            const pharmacyId = req.user.pharmacyId;

            const drug = await DrugService.modifyStock(drugId, pharmacyId, addStock, stockValue);
            return res.status(200).json(drug);
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getPharmacyDrugs(req, res) {
        try {
            const latitude = req.query.latitude;
            const longitude = req.query.longitude;
            const drugId = req.query.drugId;
            const pharmacyId = req.query.pharmacyId;
            const pharmacyDrugs = await DrugService.getPharmacyDrugs(latitude, longitude, drugId, pharmacyId);
            return res.status(200).json(pharmacyDrugs);
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async addDrugToPharmacy(req, res) {
        try {
            const stockValue = req.body.stockValue;
            const drugId = req.params.id;
            const pharmacyId = req.user.pharmacyId;

            const drug = await DrugService.addDrugToPharmacy(drugId, pharmacyId, stockValue);
            return res.status(200).json(drug);
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getDrugs(req, res) {
        try {
            const allDrugs = await DrugService.getAllDrugs();
            if (allDrugs.length > 0) {
                return res.status(200).json(allDrugs);
            } else {
                return res.status(200).json({title: "There are no drugs in database"});
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getShapes(req, res) {
        try {
            const allShapes = await DrugService.getAllShapes();
            if (allShapes.length > 0) {
                return res.status(200).json(allShapes);
            } else {
                return res.status(200).json({title: "There are no shapes in database"});
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getTags(req, res) {
        try {
            const allTags = await DrugService.getAllTags();
            if (allTags.length > 0) {
                return res.status(200).json(allTags);
            } else {
                return res.status(200).json({title: "There are no tags in database"});
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getDrugClasses(req, res) {
        try {
            const allDrugClasses = await DrugService.getAllDrugClasses();
            if (allDrugClasses.length > 0) {
                return res.status(200).json(allDrugClasses);
            } else {
                return res.status(200).json({title: "There are no drug classes in database"});
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getAllUsers(req, res) {
        try {
            const allUsers = await UserService.getAllUsers();
            if (allUsers.length > 0) {
                return res.status(200).json({title: "EVO KNJIGAw"});
            } else {
                return res.status(200).json({title: "NEMA KNJIGAw"});
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async addUser(req, res) {
        const newUser = req.body;
        try {
            newUser.password = await bcrypt.hash(newUser.password, 10)
            const createdUser = await UserService.addUser(newUser);
            return res.json(createdUser)
        } catch (error) {
            console.log(error)
        }
    }
}
