const PharmacyService = require('../services/PharmacyService');

module.exports = class PharmacyController {
    static async getPharmacy(req, res) {
        try {
            const pharmacyId = req.params.id
            const pharmacy = await PharmacyService.getPharmacy(pharmacyId);
            return res.status(200).json(pharmacy);
        } catch (error) {
            return res.status(400).json(error);
        }
    }
}
