const jwt = require('jsonwebtoken');
const passport = require('passport');
const database = require('../database/models')


module.exports = class UserController {
    // static async login(req, res) {
    //     // return res.json({kk: 'wdw'})
    //     try {
    //         const allUsers = await UserService.getAllUsers();
    //         if (allUsers.length > 0) {
    //             return res.status(200).json({title: ""});
    //         } else {
    //             return res.status(200).json({title: "w"});
    //         }
    //         return util.send(res);
    //     } catch (error) {
    //         util.setError(400, error);
    //         return util.send(res);
    //     }
    // }

    static async login(req, res) {
        // const k = await database.User.findOne({
        //     where: {
        //         email: 'ico@hotmail.com'
        //     }
        // });
        // console.log(k)
        passport.authenticate('local', {session: false}, (err, user, info) => {
            console.log(user)
            if (err || !user) {
                return res.status(400).json({
                    message: info ? info.message : 'Login failed',
                    user: user,
                    err: err
                });
            }
            req.login(user, {session: false}, (err) => {
                if (err) {
                    res.send(err);
                }
                console.log(user);
                const userData = {id: user.id, role: user.role, email: user.email, pharmacyId: user.PharmacyId}
                const token = jwt.sign(userData, process.env.JWT_SECRET_KEY, {expiresIn: '1h'});
                return res.json({userData, token});
            });
        })(req, res);
    }
}
