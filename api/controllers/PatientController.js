const PatientService = require('../services/PatientService');
const {Op} = require('sequelize')

module.exports = class DrugController {

    static async getAllPatients(req, res) {
        try {
            // return res.status(200).json(req.user.role);

            let patients;
            if (req.user.role === 'caretaker')
                patients = await PatientService.getAllPatients(req.user.id);
            else
                patients = await PatientService.getAllPatients();

            if (patients.length > 0) {
                return res.status(200).json(patients);
            } else {
                return res.status(200).json({title: "There are no patients in database"});
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async storePatient(req, res) {
        try {
            const patient = await PatientService.storeAPatient(req.body);
            if (patient) {
                return res.status(200).json(patient);
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async attachCaretaker(req, res) {
        try {
            const patientId = req.params.id;
            const attachedCaretaker = await PatientService.attachACaretaker(patientId, req.body);
            if (attachedCaretaker) {
                return res.status(200).json(attachedCaretaker);
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async detachCaretaker(req, res) {
        try {
            const patientId = req.params.id;
            const detachedCaretaker = await PatientService.detachACaretaker(patientId, req.body);
            console.log(detachedCaretaker);
            if (detachedCaretaker) {
                return res.status(200).json(detachedCaretaker);
            }
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    static async getPatient(req, res) {
        try {
            const patientId = req.params.id
            const patient = await PatientService.getPatient(patientId);
            return res.status(200).json(patient);
        } catch (error) {
            return res.status(400).json(error);
        }
    }
}
