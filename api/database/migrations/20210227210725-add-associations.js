// 'use strict';
//
// module.exports = {
//   up: (queryInterface, Sequelize) => {
//     // Order belongsTo Customer
//     drugClassId: {
//       type: Sequelize.STRING
//     },
//     shapeId: {
//       type: Sequelize.STRING
//     },
//     return queryInterface.addColumn(
//         'Drugs', // name of Source model
//         'CustomerId', // name of the key we're adding
//         {
//           type: Sequelize.Integer,
//           references: {
//             model: 'Customers', // name of Target model
//             key: 'id', // key in Target model that we're referencing
//           },
//           onUpdate: 'CASCADE',
//           onDelete: 'SET NULL',
//         }
//     )
//         .then(() => {
//           // Payment hasOne Order
//           return queryInterface.addColumn(
//               'Orders', // name of Target model
//               'PaymentId', // name of the key we're adding
//               {
//                 type: Sequelize.UUID,
//                 references: {
//                   model: 'Payments', // name of Source model
//                   key: 'id',
//                 },
//                 onUpdate: 'CASCADE',
//                 onDelete: 'SET NULL',
//               }
//           );
//         })
//   },
//
//   down: (queryInterface, Sequelize) => {
//     /*
//       Add reverting commands here.
//       Return a promise to correctly handle asynchronicity.
//
//       Example:
//       return queryInterface.dropTable('users');
//     */
//   }
// };
