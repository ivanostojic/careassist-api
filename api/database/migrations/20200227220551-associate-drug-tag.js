module.exports = {
    up: (queryInterface, Sequelize) => {
        // Product belongsToMany Tag
        return queryInterface.createTable(
            'Drug_Tag',
            {
                DrugId: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                },
                TagId: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                },
                createdAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
                updatedAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                }
            }
        );
    },

    down: (queryInterface, Sequelize) => {
        // remove table
        return queryInterface.dropTable('Drug_Tag');
    },
};
