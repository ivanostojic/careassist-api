'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Pharmacies', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING
            },
            address: {
                type: Sequelize.STRING
            },
            workingHours: {
                type: Sequelize.TEXT
            },
            contact_phone: {
                type: Sequelize.STRING,
                allowNull: true
            },
            contact_mail: {
                type: Sequelize.STRING,
                allowNull: true
            },
            longitude: {
                type: Sequelize.DOUBLE,
                allowNull: true,
            },
            latitude: {
                type: Sequelize.DOUBLE,
                allowNull: true,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Pharmacies');
    }
};
