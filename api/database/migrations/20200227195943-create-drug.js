'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Drugs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      genericName: {
        type: Sequelize.STRING
      },
      brandName: {
        type: Sequelize.STRING
      },
      drugName: {
        type: Sequelize.STRING,
        allowNull: true
      },
      year: {
        type: Sequelize.INTEGER
      },
      aboutDrug: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      drugPdf: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      DrugClassId: {
        type: Sequelize.INTEGER
      },
      ShapeId: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Drugs');
  }
};
