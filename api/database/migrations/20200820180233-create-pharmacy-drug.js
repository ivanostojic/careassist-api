'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Pharmacy_Drug', {
      pharmacyId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      drugId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      stock: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      price: {
        type: Sequelize.DECIMAL(10,2),
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Pharmacy_Drug');
  }
};
