'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Patient_Caretaker', {
            patientId: {
                type: Sequelize.INTEGER,
                primaryKey: true,
            },
            caretakerId: {
                type: Sequelize.INTEGER,
                primaryKey: true,
            },
            dateFrom: {
                type: Sequelize.DATE
            },
            dateTo: {
                allowNull: true,
                type: Sequelize.DATE
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Patient_Caretaker');
    }
};
