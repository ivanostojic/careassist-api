'use strict';
module.exports = (sequelize, DataTypes) => {
  const Shape = sequelize.define('Shape', {
    name: DataTypes.STRING
  }, {});
  Shape.associate = function(models) {
    Shape.hasOne(models.Drug);
  };
  return Shape;
};
