'use strict';
module.exports = (sequelize, DataTypes) => {
  const DrugClass = sequelize.define('DrugClass', {
    name: DataTypes.STRING
  }, {});
  DrugClass.associate = function(models) {
    DrugClass.hasOne(models.Drug);
  };
  return DrugClass;
};
