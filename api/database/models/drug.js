'use strict';
module.exports = (sequelize, DataTypes) => {
  const Drug = sequelize.define('Drug', {
    genericName: DataTypes.STRING,
    brandName: DataTypes.STRING,
    drugName: DataTypes.STRING,
    year: DataTypes.INTEGER,
    aboutDrug: DataTypes.TEXT,
    drugPdf: DataTypes.STRING
    // drugClassId: DataTypes.INTEGER,
    // shapeId: DataTypes.INTEGER
  }, {});
  Drug.associate = function(models) {
    Drug.belongsToMany(models.Tag, { through: 'Drug_Tag' });
    Drug.belongsTo(models.Shape);
    Drug.belongsTo(models.DrugClass);
    Drug.hasMany(models.PharmacyDrug, {foreignKey: 'drugId'});
    Drug.belongsToMany(models.Pharmacy, {
      as: 'pharmacies',
      through: models.PharmacyDrug,
      foreignKey: 'drugId',
      otherKey: 'pharmacyId'
    })
  };
  return Drug;
};
