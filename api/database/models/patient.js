'use strict';
module.exports = (sequelize, DataTypes) => {
    const Patient = sequelize.define('Patient', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        weight: DataTypes.DECIMAL(10, 2),
        height: DataTypes.INTEGER,
        birthDate: DataTypes.DATE,
        phone: DataTypes.STRING,
        gender: DataTypes.ENUM('M', 'F'),
        TIS: DataTypes.STRING,
        city: DataTypes.STRING,
        DNI: DataTypes.STRING
    }, {});
    Patient.associate = function (models) {
        Patient.belongsToMany(models.User, {
            as: 'caretakers',
            through: models.PatientCaretaker,
            foreignKey: 'patientId',
            otherKey: 'caretakerId'
        })
    };
    return Patient;
};
