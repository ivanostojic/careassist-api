'use strict';
module.exports = (sequelize, DataTypes) => {
  const Patient_Caretaker = sequelize.define('PatientCaretaker', {
    dateFrom: DataTypes.DATE,
    dateTo: DataTypes.DATE,
  }, {
    tableName: 'Patient_Caretaker',
  });
  Patient_Caretaker.associate = function(models) {
    // associations can be defined here
  };
  return Patient_Caretaker;
};
