module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        birthDate: DataTypes.DATE,
        email: {
            type: DataTypes.STRING,
            unique: true,
        },
        password: DataTypes.STRING,
        role: DataTypes.ENUM('pharmacist', 'caretaker', 'admin'),
        DNI: DataTypes.STRING,
        phone: DataTypes.STRING,
        city: DataTypes.STRING
    }, {
        scopes: {
            withoutPassword: {
                attributes: {exclude: ['password']},
            }
        }
    });
    User.associate = function (models) {
        // associations can be defined here
        // Patient.belongsToMany(models.User, { as: 'Caretakers', through: 'Patient_Caretaker', foreignKey: 'patientId', otherKey: 'caretakerId' })

        User.belongsToMany(models.Patient, {
            as: 'patients',
            through: models.PatientCaretaker,
            foreignKey: 'caretakerId',
            otherKey: 'patientId'
        })


        // User.hasMany(models.Post, {
        //   foreignKey: 'userId',
        //   as: 'posts',
        //   onDelete: 'CASCADE',
        // });
        //
        // User.hasMany(models.Comment, {
        //   foreignKey: 'userId',
        //   as: 'comments',
        //   onDelete: 'CASCADE',
        // });
    };
    return User;
};
