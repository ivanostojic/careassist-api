'use strict';
module.exports = (sequelize, DataTypes) => {
  const Pharmacy = sequelize.define('Pharmacy', {
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    workingHours: DataTypes.TEXT,
    contact_phone: DataTypes.STRING,
    contact_mail: DataTypes.STRING,
    longitude: DataTypes.DOUBLE,
    latitude: DataTypes.DOUBLE,
  }, {});
  Pharmacy.associate = function(models) {
    Pharmacy.belongsToMany(models.Drug, {
      as: 'drugs',
      through: models.PharmacyDrug,
      foreignKey: 'pharmacyId',
      otherKey: 'drugId'
    })
    Pharmacy.hasMany(models.User);
  };
  return Pharmacy;
};
