'use strict';
module.exports = (sequelize, DataTypes) => {
    const Pharmacy_Drug = sequelize.define('PharmacyDrug', {
        stock: DataTypes.INTEGER,
        price: DataTypes.DECIMAL(10, 2)
    }, {
        tableName: 'Pharmacy_Drug',
    });
    Pharmacy_Drug.associate = function (models) {
        Pharmacy_Drug.belongsTo(models.Pharmacy, {foreignKey: 'pharmacyId'});
        Pharmacy_Drug.belongsTo(models.Drug, {foreignKey: 'drugId'});
    };
    return Pharmacy_Drug;
};
