'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Pharmacy_Drug', [
      {
        pharmacyId: 1,
        drugId: 1,
        stock: 3,
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        pharmacyId: 2,
        drugId: 1,
        stock: 4,
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        pharmacyId: 5,
        drugId: 2,
        stock: 10,
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        pharmacyId: 6,
        drugId: 2,
        stock: 5,
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        pharmacyId: 3,
        drugId: 4,
        stock: 2,
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        pharmacyId: 4,
        drugId: 7,
        stock: 20,
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Pharmacy_Drug', null, {});
  }
};
