module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Shapes', [
      {
        id: 1,
        name: 'Pill',
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      },
      {
        id: 2,
        name: 'Syrup',
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      },
      {
        id: 3,
        name: 'Injection',
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      }
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Shapes', null, {});
  }
};
