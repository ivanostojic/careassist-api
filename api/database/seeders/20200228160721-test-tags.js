module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Tags', [
            {
                id: 1,
                name: 'Heart Disease',
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 2,
                name: 'Hypertension',
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 3,
                name: 'Asthma',
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            }
        ]);
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Tags', null, {});
    }
};
