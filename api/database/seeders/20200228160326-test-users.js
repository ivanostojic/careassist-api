module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [
      {
        id: 1,
        firstName: 'Ivan',
        lastName: 'Ostojic',
        birthDate: '1996-07-08T22:00:00.000Z',
        email: 'pharmacist@test.com',
        password: '$2b$10$Tqs8yTOdtF/I7SnNOoemrenSDDzYSZsc1.Ftrn66BvTEJ9/dPNaIi',
        role: 'pharmacist',
        DNI: '2323232545',
        phone: '2019-11-04 21:57:29',
        city: 'Mostar',
        PharmacyId: 1,
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      },
      {
        id: 2,
        firstName: 'Filip',
        lastName: 'Filipovic',
        birthDate: '1996-07-08T22:00:00.000Z',
        email: 'caretaker@test.com',
        password: '$2b$10$Tqs8yTOdtF/I7SnNOoemrenSDDzYSZsc1.Ftrn66BvTEJ9/dPNaIi',
        role: 'caretaker',
        DNI: '2323232545',
        phone: '2019-11-04 21:57:29',
        city: 'Mostar',
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      },
      {
        id: 3,
        firstName: 'Jure',
        lastName: 'Juric',
        birthDate: '1996-07-08T22:00:00.000Z',
        email: 'caretaker2@test.com',
        password: '$2b$10$Tqs8yTOdtF/I7SnNOoemrenSDDzYSZsc1.Ftrn66BvTEJ9/dPNaIi',
        role: 'caretaker',
        DNI: '2323232545',
        phone: '2019-11-04 21:57:29',
        city: 'Mostar',
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      }
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
