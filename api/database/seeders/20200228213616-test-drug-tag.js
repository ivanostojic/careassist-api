module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Drug_Tag', [
      {
        DrugId: 1,
        TagId: 1,
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      },
      {
        DrugId: 1,
        TagId: 2,
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      },
      {
        DrugId: 3,
        TagId: 3,
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      }
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Drug_Tag', null, {});
  }
};
