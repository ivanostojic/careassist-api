'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Patient_Caretaker', [
      {
        patientId: 1,
        caretakerId: 2,
        dateFrom: '2020-05-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        patientId: 2,
        caretakerId: 2,
        dateFrom: '2020-06-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        patientId: 4,
        caretakerId: 2,
        dateFrom: '2020-05-25T16:03:05.752Z',
        dateTo: '2020-06-25T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        patientId: 6,
        caretakerId: 2,
        dateFrom: '2020-05-25T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        patientId: 3,
        caretakerId: 3,
        dateFrom: '2020-04-21T16:03:05.752Z',
        dateTo: '2020-05-21T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        patientId: 4,
        caretakerId: 3,
        dateFrom: '2020-05-21T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
      {
        patientId: 5,
        caretakerId: 3,
        dateFrom: '2020-05-01T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z',
        updatedAt: '2020-02-28T16:03:05.752Z'
      },
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Patient_Caretaker', null, {});
  }
};
