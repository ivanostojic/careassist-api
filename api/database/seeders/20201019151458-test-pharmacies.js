'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Pharmacies', [
            {
                id: 1,
                name: 'Pharmacy Major 13',
                address: 'Calle Mayor, 13, 28013 Madrid, Spain',
                workingHours: '23:00 - 24:00',
                contact_phone: '+34 912 13 61 78',
                contact_mail: 'pharmacy.major@gmail.com',
                longitude: -3.705985,
                latitude: 40.416316,
                createdAt: '2020-02-28T16:03:05.752Z',
                updatedAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 2,
                name: 'Pharmacy Mejor 13',
                address: 'Calle de Triunfo, 13, 28013 Granada, Spain',
                workingHours: '07:00 - 24:00',
                contact_phone: '+34 942 23 51 78',
                contact_mail: 'pharmacy.mejor@gmail.com',
                longitude: -3.705985,
                latitude: 39.416316,
                createdAt: '2020-02-28T16:03:05.752Z',
                updatedAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 3,
                name: 'Universal Pharmacy 24H',
                address: 'Boulevard Príncipe Alfonso Von Hohenlohe, 29600 Marbella, Spain',
                workingHours: '07:00 - 24:00',
                contact_phone: '+34 952 86 75 36',
                contact_mail: 'pharmacy.universal@gmail.com',
                longitude: -3.8076397,
                latitude:  37.6742282,
                createdAt: '2020-02-28T16:03:05.752Z',
                updatedAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 4,
                name: 'Rivers Pharmacy Zarco 24',
                address: 'Puerta Real de España 2, 18005 Granada, Spain',
                workingHours: '07:00 - 24:00',
                contact_phone: '+34 952 76 15 26',
                contact_mail: 'pharmacy.zarco@gmail.com',
                longitude: -3.405985,
                latitude: 37.216316,
                createdAt: '2020-02-28T16:03:05.752Z',
                updatedAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 5,
                name: 'Pharmacy Zoco Calahonda',
                address: 'Centro Comercial El Zoco de Calahonda, 29649 Calahonda, Spain',
                workingHours: '07:00 - 24:00',
                contact_phone: '+34 952 11 25 66',
                contact_mail: 'pharmacy.zoco@gmail.com',
                longitude: -4.605985,
                latitude: 36.716316,
                createdAt: '2020-02-28T16:03:05.752Z',
                updatedAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 6,
                name: 'Pharmacy La Marina',
                address: 'Calle Amsterdam, 14, 03177 San Fulgencio, Alicante, Spain',
                workingHours: '07:00 - 24:00',
                contact_phone: '+34 952 89 15 86',
                contact_mail: 'pharmacy.marina@gmail.com',
                longitude: -0.605985,
                latitude: 38.316316,
                createdAt: '2020-02-28T16:03:05.752Z',
                updatedAt: '2020-02-28T16:03:05.752Z'
            },
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Pharmacies', null, {});
    }
};
