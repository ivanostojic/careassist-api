module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Drugs', [
            {
                id: 1,
                genericName: 'Alprazolam',
                brandName: 'Xanax',
                year: 1992,
                aboutDrug: 'Xanax (alprazolam) is a benzodiazepine (ben-zoe-dye-AZE-eh-peen). It is thought that alprazolam works by enhancing the activity of certain neurotransmitters in the brain.\n' +
                    '\n' +
                    'Xanax is a prescription medicine used to treat anxiety disorders and anxiety caused by depression.\n' +
                    '\n' +
                    'Xanax is also used to treat panic disorders with or without a fear of places and situations that might cause panic, helplessness, or embarrassment (agoraphobia).',
                drugPdf: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
                DrugClassId: 1,
                ShapeId: 2,
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 2,
                genericName: 'Ibuprofen',
                brandName: 'Advil, Midol, Motrin',
                year: 1999,
                aboutDrug: 'Ibuprofen is a nonsteroidal anti-inflammatory drug (NSAID). It works by reducing hormones that cause inflammation and pain in the body.\n' +
                    '\n' +
                    'Ibuprofen is used to reduce fever and treat pain or inflammation caused by many conditions such as headache, toothache, back pain, arthritis, menstrual cramps, or minor injury.\n' +
                    '\n' +
                    'Ibuprofen is used in adults and children who are at least 6 months old.',
                drugPdf: '/documents/pdf2.pdf',
                DrugClassId: 3,
                ShapeId: 1,
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 3,
                genericName: 'Lorazepam',
                brandName: 'Ativan',
                year: 1999,
                aboutDrug: 'Ativan (lorazepam) belongs to a group of drugs called benzodiazepines. It is thought that lorazepam works by enhancing the activity of certain neurotransmitters in the brain.\n' +
                    '\n' +
                    'Ativan is used to treat anxiety disorders and seizure disorders.\n' +
                    '\n' +
                    'It is dangerous to purchase Ativan on the Internet or outside the United States. The sale and distribution of medicines outside the U.S. does not comply with safe-use regulations of the Food and Drug Administration (FDA). These medications may contain dangerous ingredients, or may not be distributed by a licensed pharmacy.',
                drugPdf: '/documents/pdf3.pdf',
                DrugClassId: 2,
                ShapeId: 3,
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 4,
                genericName: 'Clonazepam ',
                brandName: 'KlonoPIN',
                year: 1999,
                aboutDrug: 'Clonazepam is a benzodiazepine. It is thought that clonazepam works by enhancing the activity of certain neurotransmitters in the brain.\n' +
                    '\n' +
                    'Clonazepam, a type of anti-epileptic drug, is used to treat certain seizure disorders (including absence seizures or Lennox-Gastaut syndrome) in adults and children.\n' +
                    '\n' +
                    'Clonazepam is also used to treat panic disorder (including agoraphobia) in adults.',
                drugPdf: '/documents/pdf3.pdf',
                DrugClassId: 2,
                ShapeId: 3,
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 5,
                genericName: 'Adalimumab ',
                brandName: 'Humira',
                year: 1999,
                aboutDrug: 'Humira (adalimumab) is a tumor necrosis factor (TNF) blocker that reduces the effects of a substance in the body that can cause inflammation.\n' +
                    '\n' +
                    'Humira is used to treat many inflammatory conditions in adults, such as ulcerative colitis, rheumatoid arthritis, psoriatic arthritis, ankylosing spondylitis, plaque psoriasis, and a skin condition called hidradenitis suppurativa.\n' +
                    '\n' +
                    'Humira is also used in adults and children to treat Crohn\'s disease, juvenile idiopathic arthritis or uveitis.',
                drugPdf: '/documents/pdf3.pdf',
                DrugClassId: 2,
                ShapeId: 3,
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 6,
                genericName: 'Amoxicilin',
                brandName: 'Amoxil, Trimox, Moxatag',
                year: 1999,
                aboutDrug: 'Amoxicillin is a penicillin antibiotic that fights bacteria.\n' +
                    '\n' +
                    'Amoxicillin is used to treat many different types of infection caused by bacteria, such as tonsillitis, bronchitis, pneumonia, and infections of the ear, nose, throat, skin, or urinary tract.\n' +
                    '\n' +
                    'Amoxicillin is also sometimes used together with another antibiotic called clarithromycin (Biaxin) to treat stomach ulcers caused by Helicobacter pylori infection. This combination is sometimes used with a stomach acid reducer called lansoprazole (Prevacid).',
                drugPdf: '/documents/pdf3.pdf',
                DrugClassId: 2,
                ShapeId: 3,
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            },
            {
                id: 7,
                genericName: 'Montelukast',
                brandName: 'Singulair',
                year: 1977,
                aboutDrug: 'Montelukast is a leukotriene (loo-koe-TRY-een) inhibitor. Leukotrienes are chemicals your body releases when you breathe in an allergen (such as pollen). These chemicals cause swelling in your lungs and tightening of the muscles around your airways, which can result in asthma symptoms.\n' +
                    '\n' +
                    'Montelukast is used to prevent asthma attacks in adults and children as young as 12 months old. Montelukast is also used to prevent exercise-induced bronchoconstriction (narrowing of the air passages in the lungs) in adults and children who are at least 6 years old.\n' +
                    '\n' +
                    'Montelukast is also used to treat symptoms of year-round (perennial) allergies in adults and children who are at least 6 months old. It is also used to treat symptoms of seasonal allergies in adults and children who are at least 2 years old.',
                drugPdf: '/documents/pdf3.pdf',
                DrugClassId: 2,
                ShapeId: 3,
                updatedAt: '2020-02-28T16:03:05.752Z',
                createdAt: '2020-02-28T16:03:05.752Z'
            }
        ]);
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Drugs', null, {});
    }
};
