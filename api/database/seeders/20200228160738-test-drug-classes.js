module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('DrugClasses', [
      {
        id: 1,
        name: 'Allergenics',
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      },
      {
        id: 2,
        name: 'Antidepressants',
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      },
      {
        id: 3,
        name: 'Probiotics',
        updatedAt: '2020-02-28T16:03:05.752Z',
        createdAt: '2020-02-28T16:03:05.752Z'
      }
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('DrugClasses', null, {});
  }
};
