const passport = require('passport');
const passportJWT = require("passport-jwt");
const ExtractJWT = passportJWT.ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = passportJWT.Strategy;

const bcrypt = require('bcrypt');

const database = require('../database/models');

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function (email, password, cb) {
        database.User.findOne({
            where: {
                email: email
            }
        }).then(async user => {
            if (!user) {
                return cb(null, false, {message: 'Incorrect email or password.'});
            }
            const match = await bcrypt.compare(password, user.password);
            if (match) {
                return cb(null, user, {message: 'Logged In Successfully'});
            } else {
                return cb(null, false, {message: 'Incorrect email or password.'});
            }
        })
            .catch(err => cb(err));
    }
));

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET_KEY
    },
    function (jwtPayload, cb) {
        //find the user in db if needed
        return cb(null, jwtPayload)
    }
));

