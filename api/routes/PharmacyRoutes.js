const {Router} = require('express');
const PharmacyController = require('../controllers/PharmacyController')

const router = Router();

router.get('/:id', PharmacyController.getPharmacy);

module.exports = router;
