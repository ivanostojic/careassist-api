const {Router} = require('express');
const UserController = require('../controllers/UserController')

const router = Router();

router.get('/', UserController.getAllUsers);
router.post('/', UserController.addUser);
router.get('/caretakers', UserController.getCaretakers);
router.get('/my-pharmacy', UserController.getMyPharmacy);

module.exports = router;
