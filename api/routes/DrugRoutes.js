const {Router} = require('express');
const DrugController = require('../controllers/DrugController');

const router = Router();

// router.get('/', DrugController.getAllDrugs);
router.get('/tags', DrugController.getTags);
router.get('/shapes', DrugController.getShapes);
router.get('/drug-classes', DrugController.getDrugClasses);
router.post('/search-drugs', DrugController.searchDrugs);
router.get('/pharmacy_drugs', DrugController.getPharmacyDrugs)
router.post('/:id/stock', DrugController.modifyStock);
router.post('/:id/pharmacy', DrugController.addDrugToPharmacy)

router.get('/:id', DrugController.getDrug);

module.exports = router;
