const {Router} = require('express');
// import { Router } from 'express';
const AuthController = require('../controllers/AuthController')
// import UserController from '../controllers/UserController';

const router = Router();

router.post('/login', AuthController.login);


module.exports = router;
