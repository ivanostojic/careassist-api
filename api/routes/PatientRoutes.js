const {Router} = require('express');
const PatientController = require('../controllers/PatientController');

const router = Router();

// router.get('/tags', DrugController.getTags);
router.post('/', PatientController.storePatient);
router.get('/', PatientController.getAllPatients);
router.get('/:id', PatientController.getPatient);
router.post('/:id/caretakers', PatientController.attachCaretaker);
router.put('/:id/caretakers', PatientController.detachCaretaker);

module.exports = router;
