const database = require('../database/models')

module.exports = class UserService {
    static async getAllUsers() {
        return database.User.findAll();
    }

    static async addUser(newUser) {
        try {
            return await database.User.create(newUser);
        } catch (error) {
            throw error;
        }
    }

    static async getAllCaretakers() {
        return database.User.findAll({
            where: {
                role: 'caretaker'
            }
        })
    }

    static async getMyPharmacy(pharmacyId) {
        return database.Pharmacy.findOne({
            where: {
                id: pharmacyId
            }
        })
    }

}
