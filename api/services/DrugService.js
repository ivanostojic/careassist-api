const database = require('../database/models')
const {Op} = require('sequelize')
const geolib = require('geolib')
module.exports = class DrugService {

    static async getADrug(id, pharmacyId = null) {
        return database.Drug.findOne({
            where: {
                id: Number(id)
            },
            include: [
                {
                    model: database.Shape
                },
                {
                    model: database.DrugClass
                },
                {
                    model: database.Pharmacy,
                    as: 'pharmacies',
                    through: {
                        where: {pharmacyId: pharmacyId},
                        // attributes: ['stock']
                    },
                },
                // {
                //     model: database.Pharmacy,
                //     as: 'pharmacies',
                //     through: {
                //         where: {pharmacyId: pharmacyId}
                //     }
                // },
                database.Tag
            ]
        });
    }

    static async modifyStock(id, pharmacyId, addStock, stockValue) {
        return database.PharmacyDrug.findOne({
            where: {
                drugId: Number(id),
                pharmacyId: pharmacyId
            },
        }).then(drug => {
            if (addStock) {
                return drug.increment('stock', {by: stockValue})
            } else {
                if (drug.stock >= stockValue)
                    drug.decrement('stock', {by: stockValue})
                return drug;
            }
        });
    }

    static async getPharmacyDrugs(latitude = null, longitude = null, drugId = null, pharmacyId = null) {
        let whereClause = {}
        if (drugId)
            whereClause.drugId = drugId
        if (pharmacyId)
            whereClause.pharmacyId = pharmacyId
        let pharmacyDrugs = await database.PharmacyDrug.findAll({
            where: whereClause,
            raw: true,
            nest: true,
            include: [
                database.Pharmacy,
                database.Drug
            ]
        })

        let userCoordinates = {latitude, longitude};

        pharmacyDrugs.forEach(item => {
            let distance = geolib.getDistance(userCoordinates, {
                latitude: Number(item.Pharmacy.latitude),
                longitude: Number(item.Pharmacy.longitude),
            })
            item.distance = geolib.convertDistance(distance, 'km');
        })

        return pharmacyDrugs.sort((a, b) => (a.distance - b.distance));
    }

    // static async getPharmacyDrugs() {
    //     return database.PharmacyDrug.findAll(
    //         // include: [
    //             // {
    //             //     model: database.Pharmacy
    //             // },
    //             // {
    //             //     model: database.Drug
    //             // }
    //         // ]
    //     )
    // }

    static async getFilteredDrugs(whereClause, whereClauseTags, whereClauseShapes, whereClauseDrugClasses) {
        try {
            return await database.Drug.findAll({
                where: whereClause,
                include: [
                    {
                        model: database.Shape,
                        where: whereClauseShapes
                    },
                    {
                        model: database.DrugClass,
                        where: whereClauseDrugClasses
                    },
                    {
                        model: database.Tag,
                        where: whereClauseTags
                    },
                    {
                        model: database.PharmacyDrug
                    },
                ]
            })
        } catch (error) {
            throw error;
        }
    }

    static async addDrugToPharmacy(id, pharmacyId, stockValue) {
        return database.PharmacyDrug.create({
                drugId: Number(id),
                pharmacyId: pharmacyId,
                stock: stockValue
            }
        )
    }

    static
    async getAllTags() {
        try {
            return await database.Tag.findAll();
        } catch (error) {
            throw error;
        }
    }

    static
    async getAllShapes() {
        try {
            return await database.Shape.findAll();
        } catch (error) {
            throw error;
        }
    }

    static
    async getAllDrugClasses() {
        try {
            return await database.DrugClass.findAll();
        } catch (error) {
            throw error;
        }
    }
}
