const database = require('../database/models')

module.exports = class PharmacyService {

    static async getPharmacy(pharmacyId) {
        return database.Pharmacy.findOne({
            where: {
                id: pharmacyId
            }
        })
    }
}
