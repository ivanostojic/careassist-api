const database = require('../database/models')
const {Op} = require('sequelize')

module.exports = class PatientService {

    static async getAllPatients(userId = null) {
        try {
            console.log(userId);
            if (userId === null) {
                return database.Patient.findAll();
            } else {
                let user = await database.User.findOne({
                    where: {
                        id: Number(userId)
                    }
                })
                return user.getPatients();
            }
        } catch (error) {
            throw error;
        }
    }

    static async storeAPatient(formData) {
        return database.Patient.create(formData);
    }

    static async getPatient(id) {
        return (database.Patient.findOne({
                where: {
                    id: Number(id)
                },
                include: {
                    model: database.User,
                    as: 'caretakers'
                }
            })
        );
    }

    static async attachACaretaker(patientId, formData) {
        return database.PatientCaretaker.create({
            patientId: patientId,
            caretakerId: formData.caretakerId,
            dateFrom: Date.now()
        });
    }

    static async detachACaretaker(patientId, formData) {
        let patientCaretaker = await database.PatientCaretaker.findOne({
            where: {patientId: patientId, caretakerId: formData.caretakerId}
        });

        return await patientCaretaker.update({
            dateTo: Date.now()
        });

        // return database.PatientCaretaker.update(
        //     {
        //         dateTo: Date.now()
        //     },
        //     {
        //         where: {patientId: patientId, caretakerId: formData.caretakerId}
        //     })
        //
    }
}
