const express = require('express');
const bodyParser = require('body-parser');
const config = require('dotenv').config();
const passport = require('passport');
require('./api/configuration/passport');
const authRoutes = require('./api/routes/AuthRoutes');
const userRoutes = require('./api/routes/UserRoutes');
const drugRoutes = require('./api/routes/DrugRoutes');
const patientRoutes = require('./api/routes/PatientRoutes');
const pharmacyRoutes = require('./api/routes/PharmacyRoutes');
const cors = require('cors');
const {resolve} = require("path");
const port = process.env.PORT || '3000'
const app = express();
const router = express.Router();

const db = require('./api/database/models')
// db.sequelize.sync().then(() => {
//     console.log("Drop and re-sync db.");
// });


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(cors());
app.options('*', cors());
app.use('/api/auth', authRoutes)
app.use('/api/users', passport.authenticate('jwt', {session: false}), userRoutes);
app.use('/api/drugs', passport.authenticate('jwt', {session: false}), drugRoutes);
app.use('/api/patients', passport.authenticate('jwt', {session: false}), patientRoutes);
app.use('/api/pharmacies', pharmacyRoutes);


const absolute = resolve('api/drugs')
console.log(absolute)


app.get('/', function (req, res) { res.send('Hello World!')});

// app.get('/', function(request, response){
//     console.log(absolute)
//     return response.json('kdwaodw')
// });

app.use((req, res) => {
    res.status(404).json({status: 404, title: "Not Found"});
});

app.listen(port, () => {
    console.log('Server started on port', port);
})

